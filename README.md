This is a wrapper to JWPlatform API for Yii2 Framework.
=======

[JWPlatform](http://www.jwplayer.com/hosting-and-streaming/) is Secure Video Hosting and Streaming Service.


Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist lezgro/yii2-jwplatform "*"
```

or add

```
"lezgro/yii2-jwplatform": "*"
```

to the require section of your `composer.json` file.


Setup
-----

Once the extension is installed, load the module in your application config:

```php
'components' => [
    'jwplatform' => [
        'class' => 'lezgro\yii2-jwplatform\JwPlatform',
        'apiKey' => 'your API key',
        'apiSecret' => 'your API secret',
    ],
]
```

Usage
-----

```php
echo Yii::$app->jwplatform->call('api_method', ['param_name' => 'param_value']);
```